@extends('pages.admin.theme.layout')
@section('title', 'Testimoni')
@section('breadcrumb', 'Testimoni')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[3].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Testimoni</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="row">
        <div class="col-md-3">
          <a href="{{ route('admin.testimoni.add') }}" class="btn btn-info"><i class="fas fa-plus-square me-2"></i>Tambah Data</a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-bordered border table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Testimoni</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('admin.testimoni.json') }}",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {data: 'testimonies_name', name: 'testimonies_name'},
      { data: 'text', name: 'text', },
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function deleteData(id){
    $.ajax({
      url: "{{ route('admin.testimoni') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection