@extends('pages.admin.theme.layout')
@section('title', 'Testimoni')
@section('breadcrumb', 'Testimoni')
@section('content')

<script type="text/javascript">
    document.getElementsByClassName('nav-link')[3].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <div class="d-md-flex align-items-center mb-3 mx-2">
            <div class="mb-md-0 mb-3">
                <h3 class="font-weight-bold mb-0">@if(isset($data)) Update @else Tambah @endif Testimoni</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card p-md-4 p-2">
            <form action="" method="post">
                @csrf
                <div class="form-group mb-3">
                    <label for="testimonies_name">Nama Testimoni</label>
                    <input 
                        type="text" 
                        class="form-control @error('testimonies_name') is-invalid @enderror" 
                        value="@if(isset($data)){{ $data->testimonies_name }}@else{{ old('testimonies_name') }}@endif" 
                        id="testimonies_name" 
                        name="testimonies_name"
                    >
                    @error('testimonies_name') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="testimonies_text">Isi Testimoni</label>
                    <textarea 
                        class="form-control @error('testimonies_text') is-invalid @enderror" 
                        name="testimonies_text"
                        id="testimonies_text"
                    >@if(isset($data)){{ $data->testimonies_text }}@else{{ old('testimonies_text') }}@endif</textarea>
                    @error('testimonies_text') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-info w-100">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection