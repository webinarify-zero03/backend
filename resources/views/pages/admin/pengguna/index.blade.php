@extends('pages.admin.theme.layout')
@section('title', 'Users')
@section('breadcrumb', 'Users')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[1].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Pengguna Webinarify</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="table-responsive">
        <table class="table table-bordered border text-center table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Foto Profil</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('admin.users.json') }}",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {
        data: 'photo',
        name: 'photo', 
        orderable: false, 
        searchable: false
      },
      {data: 'name', name: 'name'},
      {data: 'email', name: 'email'},
      {data: 'status', name: 'status'},
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function deleteData(id){
    $.ajax({
      url: "{{ route('admin.users') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection