@extends('pages.admin.theme.layout')
@section('title', 'Users')
@section('breadcrumb', 'Users')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[1].classList.add('active')
</script>

<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Edit Pengguna Webinarify</h3>
      </div>
    </div>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card p-md-4 p-2">
			<form method="post" action="" enctype="multipart/form-data">
			@csrf
				<div class="d-md-flex align-items-center">
					<div class="col-lg-3 col-md-4 col-12 my-md-0 my-2">
						<img 
			              src="
			                @if(!auth()->user()->photo_profile)
			                  {{ asset('') }}assets/img/team-2.jpg
			                @else
			                  {{ $data->photo_profile }}
			                @endif
			              " 
			              class="w-100 rounded" 
			              alt="photo-profile" 
			              id="photo-profile" />
			              <input type="file" name="photo_profile" id="set-photo-profile" class="form-control mt-2" accept="image/*">
					</div>
					<div class="col-lg-9 col-md-8 col-12 my-md-0 my-2">
						<div class="row mx-md-5">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Nama</strong></label>
									<input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $data->name }}">
									@error('name') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Email</strong></label>
									<input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $data->email }}">
									@error('email') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Password</strong></label>
									<input type="password" name="password" class="form-control @error('password') is-invalid @enderror" value="{{ $data->password }}">
									@error('password') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Nomor Telepon</strong></label>
									<input type="number" name="phone_number" class="form-control @error('phone_number') is-invalid @enderror" value="{{ $data->phone_number }}">
									@error('phone_number') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Pekerjaan</strong></label>
									<input type="text" name="job" class="form-control @error('job') is-invalid @enderror" value="{{ $data->job }}">
									@error('job') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Jenis Kelamin</strong></label>
									<select name="gender" class="form-control @error('gender') is-invalid @enderror">
										<option value="">-- Pilih Jenis Kelamin</option>
										<option @if( $data->gender == 'Laki - Laki' ) selected @endif value="Laki - Laki">Laki - Laki</option>
										<option @if( $data->gender == 'Perempuan' ) selected @endif value="Perempuan">Perempuan</option>
									</select>
									@error('gender') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
								</div>
							</div>
							<div class="col-md-6 col-12">
								<button class="btn btn-warning w-100" type="submit">Simpan</button>
							</div>
							@if(!$data->email_verified_at)
								<div class="col-md-6 col-12">
									<button class="btn btn-info w-100 activated" type="button">Aktifkan</button>
								</div>
							@endif
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection
@section('script')

<script type="text/javascript">
	$(function(){
		$('#set-photo-profile').on('change', function(e){
			if ($(this)[0].files && $(this)[0].files[0]) {     
	            var reader = new FileReader();    
	            reader.onload = function (e) {
	            	$('#photo-profile').attr('src', e.target.result)
	            }    
	            reader.readAsDataURL($(this)[0].files[0]);
	        } else {
	        	$('#photo-profile').attr('src', 
	        		'@if(!auth()->user()->photo_pofile) {{ asset('') }}assets/img/team-2.jpg @else {{ $data->photo_profile }} @endif'
	        	)
	        }
		})
		$('.activated').on('click', function(){
			Swal.fire({
				title: 'Konfirmasi aktivasi akun',
				text: 'Apakah anda yakin untuk mengaktivasi akun ini ?',
				icon: 'warning',
				showCancelButton: true,
  				confirmButtonColor: "#3085d6",
  				cancelButtonColor: "#d33",
  				confirmButtonText: "Ya, aktifkan akun!"
			}).then((result) => {
  				document.location.href="{{ route('admin.users') }}/activated/{{ $data->id }}"
			})
		});
	})
</script>

@endsection