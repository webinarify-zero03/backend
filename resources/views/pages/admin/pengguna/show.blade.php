@extends('pages.admin.theme.layout')
@section('title', 'Users')
@section('breadcrumb', 'Users')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[1].classList.add('active')
</script>

<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Detail Pengguna Webinarify</h3>
      </div>
    </div>
  </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="card p-md-4 p-2">
			<div class="d-md-flex align-items-center">
				<div class="col-lg-3 col-md-4 col-12 my-md-0 my-2">
					<img 
		              src="
		                @if(!auth()->user()->photo_profile)
		                  {{ asset('') }}assets/img/team-2.jpg
		                @else
		                  {{ $data->photo_profile }}
		                @endif
		              " 
		              class="w-100 rounded" 
		              alt="photo-profile" />
				</div>
				<div class="col-lg-9 col-md-8 col-12 my-md-0 my-2">
					<form class="mx-md-3">
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Nama</strong></label>
									<input type="text" name="name" class="form-control bg-white" readonly="" value="{{ $data->name }}">
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Email</strong></label>
									<input type="email" name="email" class="form-control bg-white" readonly="" value="{{ $data->email }}">
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Nomor Telepon</strong></label>
									<input type="text" name="phone_number" class="form-control bg-white" readonly="" value="{{ $data->phone_number }}">
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Pekerjaan</strong></label>
									<input type="text" name="job" class="form-control bg-white" readonly="" value="{{ $data->job }}">
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Jenis Kelamin</strong></label>
									<input type="text" name="gender" class="form-control bg-white" readonly="" value="{{ $data->gender }}">
								</div>
							</div>
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label><strong>Status</strong></label>
									<input type="text" name="status" class="form-control bg-white @if($data->email_verified_at) text-success @else text-danger @endif" readonly="" value="@if($data->email_verified_at) Active @else Non-Active @endif">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection