@extends('pages.admin.theme.layout')
@section('title', 'About')
@section('breadcrumb', 'About')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[7].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <h1>Webinarify</h1>
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card p-3">
                <div class="row">
                    <div class="col-md-4 col-12 mb-2">
                        <div class="form-group mb-3">
                            <img src="{{ $about->logo }}" alt="webinarify-logo" width="100%" id="logo-show">
                            <input type="file" class="form-control @error('logo') is-invalid @enderror mt-2" name="logo" id="logo">
                            @error('logo') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="instagram">Username Instagram</label>
                            <input type="text" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{ $about->instagram }}">
                            @error('instagram') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="whatsapp">Nomor Whatsapp</label>
                            <input type="number" class="form-control @error('whatsapp') is-invalid @enderror" name="whatsapp" value="{{ $about->whatsapp }}">
                            @error('whatsapp') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $about->email }}">
                            @error('email') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                    </div>
                    <div class="col-md-8 col-12 mb-2">
                        <div class="form-group mb-3">
                            <label for="short-description">Deskripsi Singkat Webinarify</label>
                            <textarea name="short_description" class="form-control @error('short_description') is-invalid @enderror" id="short-description"rows="3">{{ $about->short_description }}</textarea>
                            @error('short_description') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="long-description">Tentang Webinarify</label>
                            <textarea name="long_description" class="form-control @error('long_description') is-invalid @enderror" id="long-description"rows="3">{{ $about->long_description }}</textarea>
                            @error('long_description') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <button class="btn btn-success w-100">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    CKEDITOR.replace('long-description')
    $('#logo').on('change', function(e){
        if ($(this)[0].files && $(this)[0].files[0]) {     
            var reader = new FileReader();    
            reader.onload = function (e) {
                $('#logo-show').attr('src', e.target.result)
            }    
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            $('#logo-show').attr('src', "{{ $about->logo }}")
        }
    })
</script>
@endsection