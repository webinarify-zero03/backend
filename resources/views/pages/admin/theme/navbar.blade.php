<nav class="navbar navbar-main navbar-expand-lg mx-5 px-0 shadow-none rounded" id="navbarBlur" navbar-scroll="true">
  <div class="container-fluid py-1 px-2">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent mb-1 pb-0 pt-1 px-0 me-sm-6 me-5">
        <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Admin Panel</a></li>
        <li class="breadcrumb-item text-sm text-dark active" aria-current="page">@yield('breadcrumb')</li>
      </ol>
    </nav>
    <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4 mx-auto" id="navbar">
      <ul class="navbar-nav  justify-content-end ms-auto">
        <li class="nav-item d-xl-none ps-3 me-lg-0 me-3 d-flex align-items-center">
          <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </a>
        </li>
        <li class="nav-item dropdown pe-2 d-flex align-items-center">
          <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
            <img 
              src="
                @if(!auth()->user()->photo_profile)
                  {{ asset('') }}assets/img/team-2.jpg
                @else
                  {{ auth()->user()->photo_profile }}
                @endif
              " 
              class="avatar avatar-sm" 
              alt="profile" />
          </a>
          <ul class="dropdown-menu dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
            <li class="mb-2">
              <a class="dropdown-item border-radius-md" href="javascript:;">
                <div class="d-flex py-1">
                  <div class="my-auto">
                    <img 
                      src="
                        @if(!auth()->user()->photo_profile)
                          {{ asset('') }}assets/img/team-2.jpg
                        @else
                          {{ auth()->user()->photo_profile }}
                        @endif
                      " 
                      class="avatar avatar-sm border-radius-sm me-3" 
                      alt="avatar" />
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h6 class="text-sm font-weight-normal mb-1">
                      <span class="font-weight-bold">{{ auth()->user()->name }}</span>
                    </h6>
                    <p class="text-xs text-secondary mb-0 d-flex align-items-center ">
                      {{ auth()->user()->email }}
                    </p>
                  </div>
                </div>
              </a>
            </li>
            <hr>
            <li class="text-center">
              <a class="dropdown-item border-radius-md" href="{{ route('logout') }}">
                <h6 class="text-sm font-weight-normal mb-1">
                  <i class="fas fa-power-off"></i>
                  <span class="font-weight-bold">Logout</span>
                </h6>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>