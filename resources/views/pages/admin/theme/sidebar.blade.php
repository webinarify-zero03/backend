<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 bg-slate-900 fixed-start " id="sidenav-main">
  <div class="sidenav-header">
    <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
    <a class="navbar-brand d-flex align-items-center m-0" href="{{ route('admin.dashboard') }}" target="_blank">
      <span class="font-weight-bold text-lg">Webinarify</span>
    </a>
  </div>
  <div class="collapse navbar-collapse px-4  w-auto " id="sidenav-collapse-main">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link " href="{{ route('admin.dashboard') }}">
          <div class="p-2">
            <i class="fas fa-dashboard"></i>
          </div>
          <span class="nav-link-text ms-1">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.users') }}">
          <div class="p-2">
            <i class="fas fa-user-circle"></i>
          </div>
          <span class="nav-link-text ms-1">Pengguna</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.slider') }}">
          <div class="p-2">
            <i class="fas fa-image"></i>
          </div>
          <span class="nav-link-text ms-1">Slider</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.testimoni') }}">
          <div class="p-2">
            <i class="fas fa-message"></i>
          </div>
          <span class="nav-link-text ms-1">Testimoni</span>
        </a>
      </li>
      <li class="nav-item mt-2">
        <div class="d-flex align-items-center nav-link">
          <div class="p-2">
            <i class="fas fa-trophy"></i>
          </div>
          <span class="font-weight-normal text-md ms-1">Webinar</span>
        </div>
      </li>
      <li class="nav-item border-start my-0 pt-2">
        <a class="nav-link position-relative ms-0 ps-2 py-2 " href="{{ route('admin.category') }}">
          <span class="nav-link-text ms-1">Kategori Webinar</span>
        </a>
      </li>
      <li class="nav-item border-start my-0 pt-2">
        <a class="nav-link position-relative ms-0 ps-2 py-2 " href="{{ route('admin.event') }}">
          <span class="nav-link-text ms-1">Daftar Webinar</span>
        </a>
      </li>
      <li class="nav-item mt-2">
        <a class="nav-link" href="{{ route('admin.about') }}">
          <div class="p-2">
            <i class="fas fa-info-circle"></i>
          </div>
          <span class="nav-link-text ms-1">Tentang Webinarify</span>
        </a>
      </li>
      <li class="nav-item mt-2">
        <a class="nav-link" href="{{ route('admin.rekening') }}">
          <div class="p-2">
            <i class="fas fa-money-bill"></i>
          </div>
          <span class="nav-link-text ms-1">Nomor Rekening</span>
        </a>
      </li>
    </ul>
  </div>
</aside>