<script src="{{ asset('') }}assets/js/core/popper.min.js"></script>
<script src="{{ asset('') }}assets/js/core/bootstrap.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/smooth-scrollbar.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/chartjs.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/swiper-bundle.min.js" type="text/javascript"></script>
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script src="{{ asset('') }}assets/js/corporate-ui-dashboard.min.js?v=1.0.0"></script>

<script src="{{ asset('') }}assets/js/jquery/jquery.js"></script>
<script src="{{ asset('') }}assets/js/jquery-repeater/jquery-repeater.js"></script>
<script src="{{ asset('') }}assets/js/jquery-sticky/jquery-sticky.js"></script>
<script src="{{ asset('') }}assets/js/jquery-timepicker/jquery-timepicker.js"></script>
<script src="{{ asset('') }}assets/js/datatables-bs5/datatables-bootstrap5.js"></script>
<script src="{{ asset('') }}assets/js/sweetalert2.all.min.js"></script>
<script src="{{ asset('') }}assets/ckeditor/ckeditor.js"></script>

@if(Session::has('success'))
	<script type="text/javascript">
		Swal.fire({
			text: '{{ Session::get('success') }}',
			icon: 'success'
		})
	</script>
@elseif(Session::has('error'))
	<script type="text/javascript">
		Swal.fire({
			text: '{{ Session::get('error') }}',
			icon: 'error'
		})
	</script>
@endif