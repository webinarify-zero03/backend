<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    @yield('title') | Webinarify
  </title>
  <link href="{{ asset('') }}assets/js/jquery-timepicker/jquery-timepicker.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/js/datatables-bs5/datatables.bootstrap5.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/js/datatables-buttons-bs5/buttons.bootstrap5.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/js/datatables-checkboxes-jquery/datatables.checkboxes.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/js/jquery-timepicker/jquery-timepicker.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/js/datatables-responsive-bs5/responsive.bootstrap5.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Noto+Sans:300,400,500,600,700,800|PT+Mono:300,400,500,600,700" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/nucleo-svg.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/nucleo-svg.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/fontawesome/css/all.min.css" rel="stylesheet" />
  <link id="pagestyle" href="{{ asset('') }}assets/css/corporate-ui-dashboard.css?v=1.0.0" rel="stylesheet" />
</head>