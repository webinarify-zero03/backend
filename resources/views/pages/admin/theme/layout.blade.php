<!DOCTYPE html>
<html lang="en">

@include('pages.admin.theme.meta')

<body class="g-sidenav-show  bg-gray-100">

  @include('pages.admin.theme.sidebar')

  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    
    @include('pages.admin.theme.navbar')

    <div class="container-fluid py-4 px-5">

      @yield('content')

      @include('pages.admin.theme.footer')

    </div>
  </main>

  @include('pages.admin.theme.script')

  @yield('script')

</body>

</html>