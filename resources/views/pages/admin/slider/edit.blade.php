@extends('pages.admin.theme.layout')
@section('title', 'Slider')
@section('breadcrumb', 'Slider')
@section('content')

<script type="text/javascript">
    document.getElementsByClassName('nav-link')[2].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <div class="d-md-flex align-items-center mb-3 mx-2">
            <div class="mb-md-0 mb-3">
                <h3 class="font-weight-bold mb-0">Tambah Slider Event</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card p-md-4 p-2">
            <form action="" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group mb-3">
                    <label for="sliders_name">Nama Slider Event</label>
                    <input type="text" class="form-control @error('sliders_name') is-invalid @enderror" value="{{ $data->sliders_name }}" id="sliders_name" name="sliders_name">
                    @error('sliders_name') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="sliders_picture">Gambar Slider Event</label>
                    <input type="file" class="form-control @error('sliders_picture') is-invalid @enderror" id="sliders_picture" name="sliders_picture" accept="image/*">
                    @error('sliders_picture') <small class="text-danger"><em>{{ $message }}</em></small><br /> @enderror
                    <img src="{{ $data->sliders_picture }}" id="reader_slider" width="200px" class="mt-2">
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-info w-100">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#sliders_picture').on('change', function(){
            if ($(this)[0].files && $(this)[0].files[0]) {     
	            var reader = new FileReader();    
	            reader.onload = function (e) {
	            	$('#reader_slider').attr('src', e.target.result)
	            }    
	            reader.readAsDataURL($(this)[0].files[0]);
	        } else {
	        	$('#reader_slider').attr('src', "{{ $data->sliders_picture }}")
	        }
        })
    </script>
@endsection