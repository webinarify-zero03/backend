@extends('pages.admin.theme.layout')
@section('title', 'Slider')
@section('breadcrumb', 'Slider')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[2].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Slider Event</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="row">
        <div class="col-md-3">
          <a href="{{ route('admin.slider.add') }}" class="btn btn-info"><i class="fas fa-plus-square me-2"></i>Tambah Data</a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-bordered border text-center table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Slider</th>
              <th>Gambar Slider</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('admin.slider.json') }}",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {data: 'sliders_name', name: 'sliders_name'},
      {
        data: 'photo',
        name: 'photo', 
        orderable: false, 
        searchable: false
      },
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function deleteData(id){
    $.ajax({
      url: "{{ route('admin.slider') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection