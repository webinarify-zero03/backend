@extends('pages.admin.theme.layout')
@section('title', 'Kategori Webinar')
@section('breadcrumb', 'Kategori Webinar')
@section('content')

<script type="text/javascript">
    document.getElementsByClassName('nav-link')[4].classList.add('active')
    document.getElementsByClassName('nav-link')[5].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <div class="d-md-flex align-items-center mb-3 mx-2">
            <div class="mb-md-0 mb-3">
                <h3 class="font-weight-bold mb-0">Edit Kategori Webinar</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card p-md-4 p-2">
            <form action="" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group mb-3">
                    <label for="categories_name">Nama Kategori</label>
                    <input type="text" class="form-control @error('categories_name') is-invalid @enderror" value="{{ $data->categories_name }}" id="categories_name" name="categories_name">
                    @error('categories_name') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="categories_icon">Icon Kategori</label>
                    <input type="file" class="form-control @error('categories_icon') is-invalid @enderror" id="categories_icon" name="categories_icon" accept="image/*">
                    @error('categories_icon') <small class="text-danger"><em>{{ $message }}</em></small><br /> @enderror
                    <img id="reader_icon" width="200px" class="mt-2" src="{{ $data->categories_icon }}">
                </div>
                <div class="form-group mb-3">
                    <button class="btn btn-info w-100">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#categories_icon').on('change', function(){
            if ($(this)[0].files && $(this)[0].files[0]) {     
	            var reader = new FileReader();    
	            reader.onload = function (e) {
	            	$('#reader_icon').attr('src', e.target.result)
	            }    
	            reader.readAsDataURL($(this)[0].files[0]);
	        } else {
	        	$('#reader_icon').attr('src', "{{ $data->categories_icon }}")
	        }
        })
    </script>
@endsection