<!DOCTYPE html>
<html>
<head>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        .relative{
            position: relative;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body>
    <div class="relative" style="position: relative ;top: 0; left: 0;">
        <img src="{{ $event->sertificate }}" alt="" width="100%" height="100%">
        <h1 style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%)">{{ $participant->name_participant }}</h1>
    </div>
</body>
</html>
