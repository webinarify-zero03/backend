@extends('pages.admin.theme.layout')
@section('title', 'Partisipan Webinar')
@section('breadcrumb', 'Partisipan Webinar')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[4].classList.add('active')
  document.getElementsByClassName('nav-link')[6].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Partisipan Webinar</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="table-responsive">
        <style>
            table tr th, table tr td{
                text-align: center
            }
        </style>
        <table class="table table-bordered border table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>No. Hp</th>
              <th>Pendidikan</th>
              <th>Total Harga</th>
              <th>Bukti Pembayaran</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('event.participant') }}/{{ $event }}/json",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {data: 'name_participant', name: 'name_participant'},
      {data: 'email_participant', name: 'email_participant'},
      {data: 'phone_number_participant', name: 'phone_number_participant'},
      {data: 'education', name: 'education'},
      {data: 'price', name: 'price'},
      {data: 'payment_image', name: 'payment_image'},
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function approveData(id, approval){
    Swal.fire({
        title: 'Konfirmasi',
        text: `Anda yakin untuk ${approval === 0 ? `menolak` : `menyetujui`} data ini ?`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Tidak',
        confirmButtonText: "Ya",
    }).then((result) => {
        $.ajax({
            url: `{{ route('event.participant') }}/{{ $event }}/konfirmasi/${id}/${approval}`,
            method: "GET",
            success: function(response){
                if(response.success === true){
                Swal.fire({
                    text: response.message,
                    icon: 'success'
                }).then(() => {
                    table.ajax.reload()
                })
                }
            }
        })
    })
  }
  function deleteData(id){
    $.ajax({
      url: "{{ route('event.participant') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection