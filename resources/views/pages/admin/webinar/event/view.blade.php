@extends('pages.admin.theme.layout')
@section('title', 'Event Webinar')
@section('breadcrumb', 'Event Webinar')
@section('content')

<script type="text/javascript">
    document.getElementsByClassName('nav-link')[4].classList.add('active')
    document.getElementsByClassName('nav-link')[6].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <div class="d-md-flex align-items-center mb-3 mx-2">
            <div class="mb-md-0 mb-3">
                <h3 class="font-weight-bold mb-0">{{ $event->title }}</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card p-md-4 p-2">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-3">
                        <img src="{{ $event->event_banner }}" alt="banner" class="w-100">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label>Penyelenggara</label>
                        <div class="ms-3">
                            {{ $event->operator }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Waktu Event</label>
                        <div class="ms-3">
                            {{ date('d-m-Y', strtotime($event->event_date)) }} - {{ date('H:i:s', strtotime($event->event_time)) }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Pelaksanaan</label>
                        <div class="ms-3">
                            {{ $event->execution }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Harga</label>
                        <div class="ms-3">
                            Rp {{ number_format($event->price, 2, ',', '.') }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <a href="{{ route('event.participant') }}/{{ $event->id }}" class="btn btn-primary w-100">Lihat daftar peserta</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mb-3">
                        <label>Email</label>
                        <div class="ms-3">
                            <i class="fas fa-envelope me-2"></i>{{ $event->email }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Instagram</label>
                        <div class="ms-3">
                            <i class="fab fa-instagram me-2"></i>{{ $event->instagram }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Whatsapp</label>
                        <div class="ms-3">
                            <i class="fab fa-whatsapp me-2"></i>{{ $event->whatsapp_number }}
                        </div>
                    </div>
                    <div class="mb-3">
                        <label>Status</label>
                        <div class="ms-3">
                            <em>{{ $event->event_status }}</em>
                            @if( $event->event_status == 'Rejected' )
                                <br /><small>{{ $event->reason_reject }}</small>
                            @endif
                            @if( $event->event_status == 'Waiting Approval' )
                                <div class="row">
                                    <div class="col-md-6 mt-3">
                                        <button onclick="approveData('true')" class="btn btn-success w-100">
                                            Approve
                                        </button>
                                    </div>
                                    <div class="col-md-6 mt-3">
                                        <button onclick="approveData('false')" class="btn btn-danger w-100">
                                            Reject
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mb-3">
                        <label>Deskripsi</label>
                        <?= $event->description ?>
                    </div>
                    <div class="mb-3">
                        <label>Manfaat</label>
                        <?= $event->benefits ?>
                    </div>
                    <div class="mb-3">
                        <label>Prasyarat</label>
                        <?= $event->precondition ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        function approveData(approval){
            Swal.fire({
                title: 'Konfirmasi Event',
                text: `Apakah anda yakin untuk ${approval === 'true' ? `menerima` : `menolak`} event ini ?`,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Tidak',
                confirmButtonText: "Ya",
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: `Alasan ${approval === 'true' ? `diterima` : `ditolak`}`,
                        icon: "info",
                        html: `
                            <form class="text-center" action="{{ route('admin.event') }}/approve/{{ $event->id }}/${approval}" method="post" >
                                @csrf
                                <input class="form-control" name="reason"  ${approval === 'false' && `required`} type="text" />
                                ${approval === 'true' ? `<small><em>Tidak wajib diisi</em></small>` : ``}<br />
                                <button class="btn btn-primary">Simpan</button>
                            </form>
                        `,
                        showCancelButton: false,
                        showConfirmButton: false,
                    })
                }
            });
        }
    </script>
@endsection