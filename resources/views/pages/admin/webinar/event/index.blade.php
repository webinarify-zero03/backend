@extends('pages.admin.theme.layout')
@section('title', 'Event Webinar')
@section('breadcrumb', 'Event Webinar')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[4].classList.add('active')
  document.getElementsByClassName('nav-link')[6].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Event Webinar</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="row">
        <div class="col-md-3">
          <a href="{{ route('admin.event.add') }}" class="btn btn-info"><i class="fas fa-plus-square me-2"></i>Tambah Data</a>
        </div>
      </div>
      <div class="table-responsive">
        <style>
            table tr th, table tr td{
                text-align: center
            }
        </style>
        <table class="table table-bordered border table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Banner</th>
              <th>Penyelenggara</th>
              <th>Harga</th>
              <th>Tanggal Event</th>
              <th>Status Event</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('admin.event.json') }}",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {data: 'banner', name: 'banner'},
      {data: 'operator', name: 'operator'},
      {data: 'price_total', name: 'price_total'},
      {data: 'event_datetime', name: 'event_datetime'},
      {data: 'event_status', name: 'event_status'},
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function deleteData(id){
    $.ajax({
      url: "{{ route('admin.event') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection