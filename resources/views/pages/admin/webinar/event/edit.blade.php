@extends('pages.admin.theme.layout')
@section('title', 'Event Webinar')
@section('breadcrumb', 'Event Webinar')
@section('content')

<script type="text/javascript">
    document.getElementsByClassName('nav-link')[4].classList.add('active')
    document.getElementsByClassName('nav-link')[6].classList.add('active')
</script>


<div class="row">
    <div class="col-md-12">
        <div class="d-md-flex align-items-center mb-3 mx-2">
            <div class="mb-md-0 mb-3">
                <h3 class="font-weight-bold mb-0">Edit Event Webinar</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card p-md-4 p-2">
            <form action="" enctype="multipart/form-data" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group mb-3">
                            <label for="event_banner">Banner Event</label>
                            <input type="file" class="form-control @error('event_banner') is-invalid @enderror" id="event_banner" name="event_banner" accept="image/*">
                            @error('event_banner') <small class="text-danger"><em>{{ $message }}</em></small><br /> @enderror
                            <img id="reader_banner" width="100%" class="mt-2 rounded" src="{{ $event->event_banner }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-3">
                            <label for="sertificate">Sertifikat Event <small><br />(Harap kosongkan bagian tengah secara vertikal dan horizontal untuk nama)</small></label>
                            <input type="file" class="form-control @error('sertificate') is-invalid @enderror" id="sertificate" name="sertificate" accept="image/*">
                            @error('sertificate') <small class="text-danger"><em>{{ $message }}</em></small><br /> @enderror
                            <img id="reader_sertificate" width="100%" class="mt-2 rounded" src="{{ $event->sertificate }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group mb-3">
                            <label for="operator">Penyelenggara</label>
                            <input type="text" class="form-control @error('operator') is-invalid @enderror" value="{{ $event->operator }}" id="operator" name="operator">
                            @error('operator') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="title">Judul Event</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{ $event->title }}" id="title" name="title">
                            @error('title') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="category">Kategori Event</label>
                            <select class="form-control @error('category') is-invalid @enderror" value="{{ old('category') }}" id="category" name="category">
                                @foreach( $category as $cat )
                                    <option {{ $event->category == $cat->id ? 'selected' : '' }} value="{{ $cat->id }}">{{ $cat->categories_name }}</option>
                                @endforeach
                            </select>
                            @error('category') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="description">Deskripsi</label>
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="short-description"rows="3">{{ $event->description }}</textarea>
                            @error('description') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="benefits">Manfaat</label>
                            <textarea name="benefits" class="form-control @error('benefits') is-invalid @enderror" id="short-benefits"rows="3">{{ $event->benefits }}</textarea>
                            @error('benefits') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="precondition">Prasyarat</label>
                            <textarea name="precondition" class="form-control @error('precondition') is-invalid @enderror" id="short-precondition"rows="3">{{ $event->precondition }}</textarea>
                            @error('precondition') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mb-3">
                            <label for="event_date">Tanggal Event</label>
                            <input type="date" class="form-control @error('event_date') is-invalid @enderror" value="{{ $event->event_date }}" id="event_date" name="event_date">
                            @error('event_date') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mb-3">
                            <label for="event_time">Waktu Event</label>
                            <input type="time" class="form-control @error('event_time') is-invalid @enderror" value="{{ $event->event_time }}" id="event_time" name="event_time">
                            @error('event_time') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group mb-3">
                            <label>Pelaksanaan</label><br />
                            <label for="Online" class="d-flex align-items-center">
                                <input id="Online" type="radio" name="execution" {{ $event->execution == 'Online' ? 'checked' : '' }} class="me-2" value="Online"> Online
                            </label>
                            <label for="Offline" class="d-flex align-items-center">
                                <input id="Offline" type="radio" name="execution" {{ $event->execution == 'Offline' ? 'checked' : '' }} class="me-2" value="Offline"> Offline
                            </label><br />
                            @error('execution') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="price">Harga</label>
                            <input type="number" class="form-control @error('price') is-invalid @enderror" value="{{ $event->price }}" id="price" name="price">
                            @error('price') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" value="{{ $event->email }}" id="email" name="email">
                            @error('email') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="instagram">Instagram</label>
                            <input type="text" class="form-control @error('instagram') is-invalid @enderror" value="{{ $event->instagram }}" id="instagram" name="instagram">
                            @error('instagram') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <label for="whatsapp_number">Whatsapp</label>
                            <input type="number" class="form-control @error('whatsapp_number') is-invalid @enderror" value="{{ $event->whatsapp_number }}" id="whatsapp_number" name="whatsapp_number">
                            @error('whatsapp_number') <small class="text-danger"><em>{{ $message }}</em></small> @enderror
                        </div>
                        <div class="form-group mb-3">
                            <button class="btn btn-success">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    CKEDITOR.replace('description')
    CKEDITOR.replace('benefits')
    CKEDITOR.replace('precondition')
    $('#event_banner').on('change', function(){
        if ($(this)[0].files && $(this)[0].files[0]) {     
            var reader = new FileReader();    
            reader.onload = function (e) {
                $('#reader_banner').attr('src', e.target.result)
            }    
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            $('#reader_banner').attr('src', "{{ $event->event_banner }}")
        }
    })
    $('#sertificate').on('change', function(){
        if ($(this)[0].files && $(this)[0].files[0]) {     
            var reader = new FileReader();    
            reader.onload = function (e) {
                $('#reader_sertificate').attr('src', e.target.result)
            }    
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            $('#reader_sertificate').attr('src', "{{ $event->sertificate }}")
        }
    })
</script>
@endsection