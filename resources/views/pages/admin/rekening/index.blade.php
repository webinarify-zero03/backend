@extends('pages.admin.theme.layout')
@section('title', 'Nomor Rekening')
@section('breadcrumb', 'Nomor Rekening')
@section('content')

<script type="text/javascript">
  document.getElementsByClassName('nav-link')[8].classList.add('active')
</script>


<div class="row">
  <div class="col-md-12">
    <div class="d-md-flex align-items-center mb-3 mx-2">
      <div class="mb-md-0 mb-3">
        <h3 class="font-weight-bold mb-0">Daftar Nomor Rekening</h3>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card p-md-4 p-2">
      <div class="row">
        <div class="col-md-3">
          <a href="javascript:openAddModal();" class="btn btn-info"><i class="fas fa-plus-square me-2"></i>Tambah Data</a>
        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-bordered border text-center table-hovered" id="datatable">
          <thead>
            <tr>
              <th>No</th>
              <th>Nomor Rekening</th>
              <th>Tipe Rekening</th>
              <th>Atas Nama</th>
              <th>Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addRekeningModal" tabindex="-1" aria-labelledby="addRekeningModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addRekeningModalLabel">Daftar Rekening</h5>
          <button type="button" class="btn-close bg-secondary" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form method="post" action="">
            @csrf
            <input type="hidden" id="id" name="id" class="form-control" placeholder="">
            <input type="hidden" id="type" name="type" class="form-control" placeholder="ex: BRI/BNI/DANA/...">
            <div class="form-group mb-3">
                <label for="rekening_type">Tipe Rekening</label>
                <input type="text" id="rekening_type" name="rekening_type" required class="form-control" placeholder="ex: BRI/BNI/DANA/...">
            </div>
            <div class="form-group mb-3">
                <label for="rekening_number">Nomor Rekening</label>
                <input type="number" id="rekening_number" name="rekening_number" required class="form-control">
            </div>
            <div class="form-group mb-3">
                <label for="rekening_name">Atas Nama</label>
                <input type="text" id="rekening_name" name="rekening_name" required class="form-control">
            </div>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')

<script type="text/javascript">
    var modal = $('#addRekeningModal');
    function openAddModal(){
        modal.find('input[name=type]').val('');
        modal.find('input[name=rekening_type]').val('');
        modal.find('input[name=rekening_number]').val('');
        modal.find('input[name=rekening_name]').val('');

        modal.find('input[name=type]').val('store');
        modal.modal('show')
    }
  var table = $('#datatable').DataTable({
    processing: true,
    serverSide: true,
    ajax: "{{ route('admin.rekening.json') }}",
    columns: [
      {
        data: 'DT_RowIndex', 
        name: 'DT_RowIndex',
        orderable: false, 
        searchable: false
      },
      {data: 'rekening_number', name: 'rekening_number'},
      {data: 'rekening_type', name: 'rekening_type'},
      {data: 'rekening_name', name: 'rekening_name'},
      {
        data: 'action', 
        name: 'action', 
        orderable: false, 
        searchable: false
      },
    ]
  });
  function updateData(id){
    modal.find('input[name=type]').val('');
    modal.find('input[name=rekening_type]').val('');
    modal.find('input[name=rekening_number]').val('');
    modal.find('input[name=rekening_name]').val('');

    $.ajax({
        url: "{{ route('admin.rekening') }}/view/"+id,
        method: "GET",
        success: function(response){
            modal.find('input[name=type]').val('update');
            modal.find('input[name=id]').val(response.data.id);
            modal.find('input[name=rekening_type]').val(response.data.rekening_type);
            modal.find('input[name=rekening_number]').val(response.data.rekening_number);
            modal.find('input[name=rekening_name]').val(response.data.rekening_name);

            modal.modal('show')
        }
    })
  }
  function deleteData(id){
    $.ajax({
      url: "{{ route('admin.rekening') }}/delete/"+id,
      method: "GET",
      success: function(response){
        if(response.success === true){
          Swal.fire({
            text: response.message,
            icon: 'success'
          }).then(() => {
            table.ajax.reload()
          })
        }
      }
    })
  }
</script>

@endsection