
<!DOCTYPE html>
<html lang="en">

@include('pages.auth.theme.meta')

<body class="">
  	<main class="main-content mt-0">
	    <section>
		    <div class="page-header min-vh-100">
		        <div class="container">
					@yield('content')
		        </div>
		    </div>
	    </section>
  	<main>
	@include('pages.auth.theme.script')
</body>

</html>