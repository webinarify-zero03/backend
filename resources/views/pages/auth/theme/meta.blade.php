<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    Admin Panel | Login
  </title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|Noto+Sans:300,400,500,600,700,800|PT+Mono:300,400,500,600,700" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/nucleo-svg.css" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/349ee9c857.js" crossorigin="anonymous"></script>
  <link href="{{ asset('') }}assets/css/nucleo-svg.css" rel="stylesheet" />
  <link href="{{ asset('') }}assets/css/sweetalert2.min.css" rel="stylesheet" />
  <link id="pagestyle" href="{{ asset('') }}assets/css/corporate-ui-dashboard.css?v=1.0.0" rel="stylesheet" />
</head>
