<script src="{{ asset('') }}assets/js/core/popper.min.js"></script>
<script src="{{ asset('') }}assets/js/core/bootstrap.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/perfect-scrollbar.min.js"></script>
<script src="{{ asset('') }}assets/js/plugins/smooth-scrollbar.min.js"></script>
<script>
  var win = navigator.platform.indexOf('Win') > -1;
  if (win && document.querySelector('#sidenav-scrollbar')) {
    var options = {
      damping: '0.5'
    }
    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
  }
</script>
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script src="{{ asset('') }}assets/js/corporate-ui-dashboard.min.js?v=1.0.0"></script>
<script src="{{ asset('') }}assets/js/sweetalert2.all.min.js"></script>
@if(Session::has('success'))
	<script type="text/javascript">
		Swal.fire({
			text: '{{ Session::get('success') }}',
			icon: 'success'
		})
	</script>
@elseif(Session::has('error'))
	<script type="text/javascript">
		Swal.fire({
			text: '{{ Session::get('error') }}',
			icon: 'error'
		})
	</script>
@endif