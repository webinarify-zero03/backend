@extends('pages.auth.theme.layout')
@section('content')

<div class="row">
  <div class="col-xl-4 col-md-6 d-flex flex-column mx-auto">
    <div class="card card-plain border border-dark rounded">
      <div class="card-header pb-0 text-left bg-transparent">
        <h3 class="font-weight-black text-dark display-6">Welcome back</h3>
        <p class="mb-0">Welcome back! Please enter your details.</p>
      </div>
      <div class="card-body">
        <form role="form" method="post" action="">
          @csrf
          <label>Email Address</label>
          <div class="mb-3">
            <input type="email" value="{{ old('email') }}" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter your email address" aria-label="Email" aria-describedby="email-addon">
            @error('email')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
          </div>
          <label>Password</label>
          <div class="mb-3">
            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter password" aria-label="Password" aria-describedby="password-addon">
            @error('password')<small class="text-danger"><em>{{ $message }}</em></small>@enderror
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-dark w-100 mt-4 mb-3">Sign in</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="position-absolute w-md-30 w-xl-40 top-0 end-0 h-100 d-md-block d-none">
      <div class="oblique-image position-absolute fixed-top ms-auto h-100 z-index-0 bg-cover ms-n8" style="background-image:url('{{ asset('') }}assets/img/image-sign-in.jpg')"></div>
    </div>
  </div>
  </div>
</div>

@endsection