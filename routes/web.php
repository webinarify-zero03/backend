<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('pages.auth.login');
})->name('login');
Route::controller(App\Http\Controllers\AuthController::class)->group(function(){
	Route::post('/', 'loginProcess')->name('login.process');
	Route::get('/logout', 'logout')->name('logout');
});
Route::middleware(['auth'])->prefix('/admin')->group(function(){
	Route::controller(App\Http\Controllers\Admin\DashboardController::class)->group(function(){
		Route::get('/', 'index')->name('admin.dashboard');
	});
	Route::controller(App\Http\Controllers\Admin\UserController::class)->prefix('/pengguna')->group(function(){
		Route::get('/', 'index')->name('admin.users');
		Route::get('/json', 'json')->name('admin.users.json');
		Route::get('/show/{id}', 'show');
		Route::get('/edit/{id}', 'edit');
		Route::get('/activated/{id}', 'activated');
		Route::post('/edit/{id}', 'update');
		Route::get('/delete/{id}', 'destroy');
	});
	Route::controller(App\Http\Controllers\Admin\SliderController::class)->prefix('/slider')->group(function(){
		Route::get('/', 'index')->name('admin.slider');
		Route::get('/json', 'json')->name('admin.slider.json');
		Route::get('/add', 'add')->name('admin.slider.add');
		Route::post('/add', 'store')->name('admin.slider.store');
		Route::get('/edit/{id}', 'edit');
		Route::post('/edit/{id}', 'update');
		Route::get('/delete/{id}', 'destroy');
	});
	Route::controller(App\Http\Controllers\Admin\TestimonyController::class)->prefix('/testimoni')->group(function(){
		Route::get('/', 'index')->name('admin.testimoni');
		Route::get('/json', 'json')->name('admin.testimoni.json');
		Route::get('/add', 'add')->name('admin.testimoni.add');
		Route::post('/add', 'store')->name('admin.testimoni.store');
		Route::get('/edit/{id}', 'edit');
		Route::post('/edit/{id}', 'update');
		Route::get('/delete/{id}', 'destroy');
	});
	Route::prefix('/webinar')->group(function(){
		Route::get('/', function(){ return redirect()->to(route('admin.category')); });
		Route::controller(App\Http\Controllers\Admin\CategoryController::class)->prefix('/category')->group(function(){
			Route::get('/', 'index')->name('admin.category');
			Route::get('/json', 'json')->name('admin.category.json');
			Route::get('/add', 'add')->name('admin.category.add');
			Route::post('/add', 'store')->name('admin.category.store');
			Route::get('/edit/{id}', 'edit');
			Route::post('/edit/{id}', 'update');
			Route::get('/delete/{id}', 'destroy');
		});
		Route::controller(App\Http\Controllers\Admin\EventController::class)->prefix('/event')->group(function(){
			Route::get('/', 'index')->name('admin.event');
			Route::get('/json', 'json')->name('admin.event.json');
			Route::get('/add', 'add')->name('admin.event.add');
			Route::post('/add', 'store')->name('admin.event.store');
			Route::get('/view/{id}', 'view');
			Route::get('/edit/{id}', 'edit');
			Route::post('/edit/{id}', 'update');
			Route::post('/approve/{id}/{approval}', 'approve');
			Route::get('/delete/{id}', 'destroy');

			Route::controller(App\Http\Controllers\Admin\WebinarParticipant::class)->prefix('/participant')->group(function(){
				Route::get('/')->name('event.participant');
				Route::get('/{event_id}', 'index');
				Route::get('/{event_id}/json', 'json');
				Route::get('/{event_id}/konfirmasi/{id}/{status}', 'confirmation');
				Route::get('/delete/{id}', 'destroy');
				Route::get('/{event_id}/sertificate/{id}', 'sertificate');
			});
		});
	});
	Route::controller(App\Http\Controllers\Admin\AboutController::class)->prefix('/about')->group(function(){
		Route::get('/', 'index')->name('admin.about');
		Route::post('/', 'update')->name('admin.about.update');
	});
	Route::controller(App\Http\Controllers\Admin\RekeningController::class)->prefix('/rekening-account')->group(function(){
		Route::get('/', 'index')->name('admin.rekening');
		Route::get('/json', 'json')->name('admin.rekening.json');
		Route::get('/view/{id}', 'view');
		Route::post('/', 'update')->name('admin.rekening.update');
		Route::get('/delete/{id}', 'destroy');
	});
});

Route::get('/home/{id}', [\App\Http\Controllers\API\AuthAPIController::class, 'index']);
Route::get('/test', [\App\Http\Controllers\TestController::class, 'testMail'])->name('test');