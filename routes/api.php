<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(\App\Http\Controllers\API\AuthAPIController::class)->group(function(){
    Route::post('/auth/login', 'login')->middleware('emailVerificationCheck')->name('api.auth.login');
    Route::post('/auth/register', 'register')->name('api.auth.register');
    Route::get('/email/verify', 'verificationShow')->name('verification.notice');
    Route::get('/email/verify/{id}/{hash}', 'verificationEmail')->name('verification.verify');
    Route::middleware('check.token')->group(function(){
        Route::get('/user/profile', 'profile')->name('api.user.profile');
        Route::patch('/user/profile', 'profileUpdate')->name('api.user.profile.update');
        Route::get('/auth/logout', 'logout')->name('api.auth.logout');
    });    
});

Route::get('/slider-events', [App\Http\Controllers\API\SliderEventController::class, 'index'])->name('api.slider');
Route::get('/testimony', [App\Http\Controllers\API\TestimonyController::class, 'index'])->name('api.testimony');
Route::get('/about', [App\Http\Controllers\API\AboutAPIController::class, 'index'])->name('api.about');
Route::controller(App\Http\Controllers\API\CategoryController::class)->prefix('/webinar-category')->group(function(){
	Route::get('/', 'index')->name('api.category');
	Route::get('/{id}', 'show');
});