<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SliderEvent extends Model
{
    use HasFactory;
    protected $table = 'slider_events';
    protected $primaryKey = 'id';
    protected $fillable = ['sliders_name', 'sliders_picture'];
}
