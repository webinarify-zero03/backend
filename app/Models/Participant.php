<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;
    protected $table = 'participants';
    protected $primarykey = 'id';
    protected $fillable = [
        'event_id',
        'user_id',
        'name_participant',
        'email_participant',
        'phone_number_participant',
        'education',
        'price_total',
        'status',
        'payment',
    ];
}
