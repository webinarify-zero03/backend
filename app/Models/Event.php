<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $table = 'events';
    protected $primarykey = 'id';
    protected $fillable = [
        'user_id',
        'operator',
        'event_banner',
        'title',
        'description',
        'benefits',
        'precondition',
        'event_date',
        'event_time',
        'execution',
        'price',
        'email',
        'instagram',
        'whatsapp_number',
        'category',
        'event_status',
        'reason_reject',
        'sertificate',
    ];
}
