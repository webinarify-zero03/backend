<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    use HasFactory;
    protected $table = 'rekenings';
    protected $primarykey = 'id';
    protected $fillable = [
        'rekening_number',
        'rekening_type',
        'rekening_name',
    ];
}
