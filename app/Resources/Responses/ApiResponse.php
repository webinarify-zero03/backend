<?php

namespace App\Resources\Responses;

class ApiResponse
{
    public function successResponse($message, $data, $kode)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data
        ], $kode);
    }
    public function successResponseWithToken($message, $data, $token, $kode)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
            'token' => $token,
            'data' => $data
        ], $kode);
    }
    public function successDelete($message, $kode)
    {
        return response()->json([
            'success' => true,
            'message' => $message,
        ], $kode);
    }
    public function errorResponse($message, $kode)
    {
        return response()->json([
            'success' => false,
            'message' => $message
        ], $kode);
    }
    public function errorValidation($message, $errors, $kode)
    {
        return response()->json([
            'success' => false,
            'message' => $message,
            'errors' => $errors
        ], $kode);
    }
}