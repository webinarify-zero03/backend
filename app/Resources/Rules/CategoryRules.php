<?php

namespace App\Resources\Rules;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Link;

class CategoryRules
{
    public function __rules($request, $action)
    {
        if ($action == 'create' || $action == 'Create'){
            $request->validate([
                'categories_icon' => 'required|image',
                'categories_name' => 'required'
            ],[
                'categories_icon.required' => 'Icon kategori tidak boleh kosong',
                'categories_icon.image' => 'Icon kategori harus berupa gambar',
                'categories_name.required' => 'Nama kategori tidak boleh kosong'
            ]);
        } elseif ($action == 'update' || $action == 'Update'){
            $request->validate([
                'categories_icon' => 'image',
                'categories_name' => 'required'
            ],[
                'categories_icon.image' => 'Icon kategori harus berupa gambar',
                'categories_name.required' => 'Nama kategori tidak boleh kosong'
            ]);
        }
    }

    public function __upload($file)
    {
        $namafile = time().'_'.$file->getClientOriginalName();
        Storage::disk('local')->put('/public/images/webinar/category/'.$namafile, File::get($file));
        $saveFile = url('/storage/images/webinar/category/'.$namafile);
        return $saveFile;
    }
}