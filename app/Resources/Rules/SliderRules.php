<?php

namespace App\Resources\Rules;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Link;

class SliderRules
{
    public function __storeRules($request)
    {
        $request->validate([
            'sliders_name' => 'required',
            'sliders_picture' => 'required|image'
        ],[
            'sliders_name.required' => 'Nama slider tidak boleh kosong',
            'sliders_picture.required' => 'Gambar slider tidak boleh kosong',
            'sliders_picture.image' => 'Gambar slider harus berformat jpg/jpeg/png',
        ]);
    }
    public function __updateRules($request)
    {
        $request->validate([
            'sliders_name' => 'required',
            'sliders_picture' => 'image'
        ],[
            'sliders_name.required' => 'Nama slider tidak boleh kosong',
            'sliders_picture.image' => 'Gambar slider harus berformat jpg/jpeg/png',
        ]);
    }

    public function __uploadPhoto($file)
    {
        $namafile = time().'_'.$file->getClientOriginalName();
        Storage::disk('local')->put('/public/images/slider/'.$namafile, File::get($file));
        $saveFile = url('/storage/images/slider/'.$namafile);
        return $saveFile;
    }
}