<?php

namespace App\Resources\Rules;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Link;

use App\Repositories\AboutRepository;

class AboutRules
{
    private $repository;
    public function __construct(AboutRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __updateRules($request)
    {
        $request->validate([
            'logo' => 'image',
            'instagram' => 'required',
            'whatsapp' => 'required',
            'email' => 'required|email',
            'short_description' => 'required',
            'long_description' => 'required',
        ],[
            'instagram.required' => 'Akun instagram tidak boleh kosong',
            'whatsapp.required' => 'Akun whatsapp tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'short_description.required' => 'Deskripsi singkat tidak boleh kosong',
            'long_description.required' => 'Deskripsi panjang tidak boleh kosong',
            'logo.image' => 'Logo harus berformat gambar',
            'email.email' => 'Email tidak valid',
        ]);
    }

    public function __uploadPhoto($file)
    {
        $about = $this->repository->getById(1);
        if($file){
            $namafile = time().'_'.$file->getClientOriginalName();
            Storage::disk('local')->put('/public/images/logo/'.$namafile, File::get($file));
            $saveFile = url('/storage/images/logo/'.$namafile);
        } else {
            $saveFile = $about->logo;
        }
        return $saveFile;
    }
}