<?php

namespace App\Resources\Rules;

use Validator;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Repositories\UserRepository;

class UserRules
{
	private $repository;
	public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
	}

	public function __rulesUpdate($request)
	{
		$validator = $request->validate([
			'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'job' => 'required',
            'phone_number' => 'required',
            'gender' => 'required'
		],[
			'name.required' => 'Nama tidak boleh kosong',
			'email.required' => 'Email tidak boleh kosong',
			'password.required' => 'Password tidak boleh kosong',
			'job.required' => 'Pekerjaan tidak boleh kosong',
			'phone_number.required' => 'Nomor telepon tidak boleh kosong',
			'gender.required' => 'Jenis kelamin tidak boleh kosong',
			'email.email' => 'Email tidak valid',
		]);
		$user = $this->repository->getById($request->id);
		if($request->email != $user->email){
			$validator = $request->validate([
				'email' => 'unique:users'
			],[
				'email.unique' => 'Email sudah pernah digunakan'
			]);
		}
		return $validator;
	}
	public function __apiUpdate($request, $id)
	{
		$user = $this->repository->getById($id);
		$rules = [
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'job' => 'required',
			'phone_number' => 'required',
			'gender' => 'required'
		];
		$messages = [
			'name.required' => 'Nama tidak boleh kosong',
			'email.required' => 'Email tidak boleh kosong',
			'password.required' => 'Password tidak boleh kosong',
			'job.required' => 'Pekerjaan tidak boleh kosong',
			'phone_number.required' => 'Nomor telepon tidak boleh kosong',
			'gender.required' => 'Jenis kelamin tidak boleh kosong',
			'email.email' => 'Email tidak valid',
		];

		if (!$user->photo_profile) {
			$rules['photo_profile'] = 'required|image';
			$messages['photo_profile.required'] = 'Foto profil tidak boleh kosong';
			$messages['photo_profile.image'] = 'Foto profil harus dalam format gambar';
		} else {
			$rules['photo_profile'] = 'image';
			$messages['photo_profile.image'] = 'Foto profil harus dalam format gambar';
		}

		if ($request['email'] != $user['email']) {
			$rules['email'] = 'unique:users';
			$messages['email.unique'] = 'Email sudah pernah digunakan';
		}

		$validator = Validator::make($request, $rules, $messages);
		return $validator;
	}
	public function __uploadPhoto($file)
	{
        $namafile = time().'_'.$file->getClientOriginalName();
        $move = Storage::disk('local')->put('/public/images/profile/'.$namafile, File::get($file));
        $saveFile = url('/storage/images/profile/'.$namafile);
        return $saveFile;

	}
}