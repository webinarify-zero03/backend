<?php

namespace App\Resources\Rules;

use Validator;

class AuthRules
{
	public function __apiRegister($request)
	{
		$validator = Validator::make($request, [
			'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
		],[
			'name.required' => 'Nama tidak boleh kosong',
			'email.required' => 'Email tidak boleh kosong',
			'password.required' => 'Password tidak boleh kosong',
			'password_confirmation.required' => 'Konfirmasi password tidak boleh kosong',
			'email.email' => 'Email tidak valid',
			'password.confirmed' => 'Password dan konfirmasi password tidak sama',
			'email.unique' => 'Email sudah pernah digunakan',
		]);

		return $validator;
	}
	public function __apiLogin($request)
	{
		$validator = Validator::make($request, [
            'email' => 'required|email',
            'password' => 'required'
		],[
			'email.required' => 'Email tidak boleh kosong',
			'password.required' => 'Password tidak boleh kosong',
			'email.email' => 'Email tidak valid',
		]);

		return $validator;
	}
	public function __rulesLogin($request)
	{
		$validator = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
		],[
			'email.required' => 'Email tidak boleh kosong',
			'password.required' => 'Password tidak boleh kosong',
			'email.email' => 'Email tidak valid',
		]);
		return $validator;
	}
}