<?php

namespace App\Resources\Rules;

class TestimonyRules
{
    public function __rules($request)
    {
        $request->validate([
            'testimonies_name' => 'required',
            'testimonies_text' => 'required'
        ],[
            'testimonies_name.required' => 'Nama testimoni tidak boleh kosong',
            'testimonies_text.required' => 'Teks testimoni tidak boleh kosong'
        ]);
    }
}