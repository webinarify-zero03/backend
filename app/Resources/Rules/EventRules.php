<?php

namespace App\Resources\Rules;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Link;

class EventRules
{
    public function __storeRules($request)
    {
        $request->validate([
            'operator' => 'required',
            'event_banner' => 'required|image',
            'sertificate' => 'required|image',
            'title' => 'required',
            'description' => 'required',
            'benefits' => 'required',
            'precondition' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'execution' => 'required',
            'price' => 'required',
            'category' => 'required',
            'email' => 'required|email',
            'instagram' => 'required',
            'whatsapp_number' => 'required',
        ],[
            'operator.required' => 'Penyelenggara tidak boleh kosong',
            'event_banner.required' => 'Banner tidak boleh kosong',
            'sertificate.required' => 'Sertifikat tidak boleh kosong',
            'title.required' => 'Judul tidak boleh kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'benefits.required' => 'Manfaat tidak boleh kosong',
            'precondition.required' => 'Prasyarat tidak boleh kosong',
            'event_date.required' => 'Tanggal event tidak boleh kosong',
            'event_time.required' => 'Waktu event tidak boleh kosong',
            'execution.required' => 'Pilih salah satu',
            'price.required' => 'Harga tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'instagram.required' => 'Instagram tidak boleh kosong',
            'whatsapp_number.required' => 'Nomor whatsapp tidak boleh kosong',
            'event_banner.image' => 'Banner harus berupa gambar',
            'category.required' => 'Pilih salah satu kategori',
            'sertificate.image' => 'Sertifikat harus berupa gambar',
            'email.email' => 'Email tidak valid',
        ]);
    }
    public function __updateRules($request)
    {
        $request->validate([
            'operator' => 'required',
            'event_banner' => 'image',
            'sertificate' => 'image',
            'title' => 'required',
            'description' => 'required',
            'benefits' => 'required',
            'precondition' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'execution' => 'required',
            'price' => 'required',
            'category' => 'required',
            'email' => 'required|email',
            'instagram' => 'required',
            'whatsapp_number' => 'required',
        ],[
            'operator.required' => 'Penyelenggara tidak boleh kosong',
            'title.required' => 'Judul tidak boleh kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'benefits.required' => 'Manfaat tidak boleh kosong',
            'precondition.required' => 'Prasyarat tidak boleh kosong',
            'event_date.required' => 'Tanggal event tidak boleh kosong',
            'event_time.required' => 'Waktu event tidak boleh kosong',
            'execution.required' => 'Pilih salah satu',
            'price.required' => 'Harga tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'instagram.required' => 'Instagram tidak boleh kosong',
            'whatsapp_number.required' => 'Nomor whatsapp tidak boleh kosong',
            'category.required' => 'Pilih salah satu kategori',
            'event_banner.image' => 'Banner harus berupa gambar',
            'sertificate.image' => 'Sertifikat harus berupa gambar',
            'email.email' => 'Email tidak valid',
        ]);
    }

    public function __uploadPhoto($file)
    {
        $namafile = time().'_'.$file->getClientOriginalName();
        Storage::disk('local')->put('/public/images/webinar/events/'.$namafile, File::get($file));
        $saveFile = url('/storage/images/webinar/events/'.$namafile);
        return $saveFile;
    }
}