<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
	public function getAll()
	{
		return Category::all();
	}

	public function getById($id)
	{
		return Category::find($id);
	}

	public function createData(array $data)
	{
		return Category::create($data);
	}

	public function updateData(array $data, $id)
	{
		return Category::find($id)->update($data);
	}

	public function deleteData($id)
	{
		return Category::find($id)->delete();
	}
}