<?php

namespace App\Repositories;

use App\Models\SliderEvent;

class SliderEventRepository
{
	public function getById($id)
	{
		return SliderEvent::find($id);
	}
	public function getAll()
	{
		return SliderEvent::all();
	}
	public function createData(array $data)
	{
		return SliderEvent::create($data);
	}
	public function updateData(array $data, $id)
	{
		return SliderEvent::find($id)->update($data);
	}
	public function deleteData($id)
	{
		return SliderEvent::find($id)->delete();
	}
}