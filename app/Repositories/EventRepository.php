<?php

namespace App\Repositories;

use App\Models\Event;

class EventRepository
{
	public function getAll()
	{
		return Event::orderBy('id', 'DESC')->get();
	}

	public function getById($id)
	{
		return Event::find($id);
	}

	public function createData(array $data)
	{
		return Event::create($data);
	}

	public function updateData(array $data, $id)
	{
		return Event::find($id)->update($data);
	}

	public function deleteData($id)
	{
		return Event::find($id)->delete();
	}
}