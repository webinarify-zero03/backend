<?php

namespace App\Repositories;

use App\Models\Rekening;

class RekeningRepository
{
	public function getById($id)
	{
		return Rekening::find($id);
	}
	public function getAll()
	{
		return Rekening::all();
	}
	public function createData(array $data)
	{
		return Rekening::create($data);
	}
	public function updateData(array $data, $id)
	{
		return Rekening::find($id)->update($data);
	}
	public function deleteData($id)
	{
		return Rekening::find($id)->delete();
	}
}