<?php

namespace App\Repositories;
use App\Models\User;

class UserRepository
{
    public function getById($id)
    {
        return User::find($id);
    }
    public function getByRole($role)
    {
    	return User::where('role', $role)->get();
    }
    public function createData($data)
    {
        return User::create($data);
    }
    public function updateData(array $data, $id)
    {
        return User::find($id)->update($data);
    }
    public function deleteData($id)
    {
        return User::find($id)->delete();
    }
}