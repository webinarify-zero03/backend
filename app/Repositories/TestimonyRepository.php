<?php

namespace App\Repositories;

use App\Models\Testimony;

class TestimonyRepository
{
	public function getAll()
	{
		return Testimony::all();
	}
	public function getById($id)
	{
		return Testimony::find($id);
	}
	public function createData(array $data)
	{
		return Testimony::create($data);
	}
	public function updateData(array $data, $id)
	{
		return Testimony::find($id)->update($data);
	}
	public function deleteData($id)
	{
		return Testimony::find($id)->delete();
	}
}