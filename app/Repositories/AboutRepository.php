<?php

namespace App\Repositories;
use App\Models\About;

class AboutRepository
{
    public function getById($id)
    {
        return About::find($id);
    }
    public function updateData(array $data, $id)
    {
        return About::find($id)->update($data);
    }
}