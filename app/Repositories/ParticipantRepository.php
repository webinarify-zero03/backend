<?php

namespace App\Repositories;

use App\Models\Participant;

class ParticipantRepository
{
	public function getByEventId($id)
	{
		return Participant::where('event_id', $id)->get();
	}

    public function getById($id)
	{
		return Participant::find($id);
	}

	public function createData(array $data)
	{
		return Participant::create($data);
	}

	public function updateData(array $data, $id)
	{
		return Participant::find($id)->update($data);
	}

	public function deleteData($id)
	{
		return Participant::find($id)->delete();
	}
}