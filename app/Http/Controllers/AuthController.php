<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Resources\Rules\AuthRules;

use Auth;

class AuthController extends Controller
{
	private $rules;
    public function __construct(
        AuthRules $rules
    )
    {
        $this->rules = $rules;
    }

    public function loginProcess(Request $request)
    {
    	$this->rules->__rulesLogin($request);
    	$credentials = $request->only('email', 'password');
    	if(Auth::attempt($credentials)){
    		if(auth()->user()->role != 1){
    			Auth::logout();
    			return back()->with('error', 'Akun anda tidak terdaftar sebagai admin');
    		}
    		return redirect()->intended(route('admin.dashboard'));
    	} else {
    		return back()->with('error', 'Email atau password salah');
    	}
    }

    public function logout(Request $request)
    {
    	Auth::logout();
    	return redirect()->to(route('login'))->with('success', 'Anda berhasil logout');
    }
}
