<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\CategoryRepository;
use App\Resources\Responses\ApiResponse;

class CategoryController extends Controller
{
    private $repository, $response;
    public function __construct(CategoryRepository $repository, ApiResponse $response)
    {
    	$this->repository = $repository;
    	$this->response = $response;
    }

    public function index()
    {
    	$data = $this->repository->getAll();
    	return $this->response->successResponse('Data kategori berhasil didapatkan', $data, 200);
    }

    public function show(Request $request)
    {
    	$data = $this->repository->getById($request->id);
    	return $this->response->successResponse('Data kategori berhasil didapatkan', $data, 200);
    }
}
