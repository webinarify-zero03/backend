<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\SliderEventRepository;
use App\Resources\Responses\ApiResponse;

class SliderEventController extends Controller
{
	private $repository, $response;
    public function __construct(SliderEventRepository $repository, ApiResponse $response)
    {
    	$this->repository = $repository;
    	$this->response = $response;
    }

    public function index()
    {
    	$data = $this->repository->getAll();
    	return $this->response->successResponse('Data slider berhasil didapatkan', $data, 200);
    }
}
