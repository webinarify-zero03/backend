<?php

namespace App\Http\Controllers\API;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;
use App\Resources\Responses\ApiResponse;
use App\Resources\Rules\AuthRules;
use App\Resources\Rules\UserRules;

use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth, Log;

class AuthAPIController extends Controller
{
    private $userRepository, $response, $rules;
    public function __construct(
        UserRepository $userRepository, 
        ApiResponse $response, 
        AuthRules $rules,
        UserRules $userRules
    )
    {
        $this->middleware('emailVerificationCheck')->only('login');
        $this->userRepository = $userRepository;
        $this->response = $response;
        $this->rules = $rules;
        $this->userRules = $userRules;
        $this->middleware('check.token', ['only' => ['profile']]);
    }
    public function login(Request $request)
    {
        $validator = $this->rules->__apiLogin($request->all());
        if($validator->fails()){
            return $this->response->errorValidation('Validasi gagal', $validator->errors(), 422);
        }
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return $this->response->errorResponse('Email atau password tidak sesuai', 403);
        }

        $user = Auth::user();
        if(!$user->email_verified_at){
            Auth::logout();
            return $this->response->errorResponse('Email belum diverifikasi', 403);
        }
        return $this->response->successResponseWithToken('Berhasil login', $user, $token, 200);
    }

    public function register(Request $request)
    {
        $validator = $this->rules->__apiRegister($request->all());
        if($validator->fails()){
            return $this->response->errorValidation('Validasi gagal', $validator->errors(), 422);
        }
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => 2
        ];
        $create = $this->userRepository->createData($data);
        event(new Registered($create));
        return $this->response->successResponse('Data user berhasil didaftarkan', $create, 201);
    }

    public function verificationEmail(Request $request)
    {
        $user = $this->userRepository->getById($request->id);
        if($user->markEmailAsVerified()){
            event(new Verified($user));
        }
        return $this->response->successResponse('Data user berhasil diverifikasi', $user, 200);
    }

    public function profile(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $this->response->successResponse('Data user berhasil didapatkan', $user, 200);
    }

    public function profileUpdate(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $validator = $this->userRules->__apiUpdate($request->all(), $user['id']);
        if($validator->fails()){
            return $this->response->errorValidation('Validasi gagal', $validator->errors(), 422);
        }
        $object = (object)$request;
        $password = $this->setPassword($object->password);
        $photo = $this->setPhoto($object->file('photo_profile'));
        $data = [
            'photo_profile' => $photo,
            'name' => $request->name,
	        'email' => $request->email,
            'password' => $password,
	        'phone_number' => $request->phone_number,
	        'job' => $request->job,
	        'gender' => $request->gender,
        ];
        $update = $this->userRepository->updateData($data, $user->id);
    	if($update){
    		$this->response->successResponse('Data user berhasil diupdate', $data, 200);
    	}
    }

    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate(JWTAuth::parseToken());
            return $this->response->successDelete('Logout berhasi', 200);
        } catch (Exception $e) {
            return $this->response->errorResponse('Logout gagal', 500);
        }
    }
    public function setPassword($password){
        $user = JWTAuth::parseToken()->authenticate();
        if($password == $user->password){
            return $password;
        } else {
            return bcrypt($password);
        }
    }
    public function setPhoto($file){
        $user = JWTAuth::parseToken()->authenticate();
        if(!$file){
            return $user->photo_profile;
        } else {
            return $this->userRules->__uploadPhoto($file);
        }
    }
}
