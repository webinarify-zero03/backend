<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\AboutRepository;
use App\Resources\Responses\ApiResponse;
use App\Resources\Rules\AboutRules;

class AboutAPIController extends Controller
{
    private $aboutRepository, $response, $rules;
    public function __construct(AboutRepository $aboutRepository, ApiResponse $response, AboutRules $rules)
    {
        $this->aboutRepository = $aboutRepository;
        $this->response = $response;
        $this->rules = $rules;
    }

    public function index()
    {
        $data = $this->aboutRepository->getById(1);
        return $this->response->successResponse('Data tentang webinarify berhasil ditemukan', $data, 200);
    }
}
