<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\TestimonyRepository;
use App\Resources\Responses\ApiResponse;

class TestimonyController extends Controller
{
    private $repository, $response;
    public function __construct(TestimonyRepository $repository, ApiResponse $response)
    {
    	$this->repository = $repository;
    	$this->response = $response;
    }

    public function index()
    {
    	$data = $this->repository->getAll();
    	return $this->response->successResponse('Data testimoni berhasil didapatkan', $data, 200);
    }
}
