<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

class TestController extends Controller
{
    public function testMail(Request $request){
        Mail::to('ryukokaru@gmail.com')->send(new testMail());
        return 'hello world';
    }
}
