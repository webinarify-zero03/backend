<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\AboutRepository;
use App\Resources\Rules\AboutRules;
use App\Resources\Responses\ApiResponse;

use DataTables;

class AboutController extends Controller
{
    private $repository, $rules, $response;
	public function __construct(AboutRepository $repository, AboutRules $rules, ApiResponse $response, )
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
	}
    public function index()
    {
        $about = $this->repository->getById(1);
        return view('pages.admin.about.index', compact(['about']));
    }
    public function update(Request $request)
    {

        $this->rules->__updateRules($request);
        $logo = $this->rules->__uploadPhoto($request->file('logo'));
        $data = [
            'logo' => $logo,
            'instagram' => $request->instagram,
            'whatsapp' => $request->whatsapp,
            'email' => $request->email,
            'short_description' => $request->short_description,
            'long_description' => $request->long_description,
        ];
        $update = $this->repository->updateData($data, 1);
        if($update){
            return back()->with('success', 'Data tentang webinarify berhasil diupdate');
        }
    }
}
