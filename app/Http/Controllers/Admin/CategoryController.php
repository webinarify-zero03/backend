<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\CategoryRepository;
use App\Resources\Rules\CategoryRules;
use App\Resources\Responses\ApiResponse;

use DataTables;

class CategoryController extends Controller
{
    private $repository, $rules, $response;
	public function __construct(CategoryRepository $repository, CategoryRules $rules, ApiResponse $response, )
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
	}
    public function index()
    {
        return view('pages.admin.webinar.category.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getAll();
	        return Datatables::of($data)
	            ->addIndexColumn()
                ->addColumn('icon', function($row){
                    return '
                        <img src="'.$row['categories_icon'].'" width="75" />
                    ';
                })
	            ->addColumn('action', function($row){
	                return '
	                	<a href="'.route('admin.category').'/edit/'.$row['id'].'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['icon', 'action'])
	            ->make(true);
	    }
    }
    public function add()
    {
        return view('pages.admin.webinar.category.add');
    }
    public function store(Request $request)
    {
        $this->rules->__rules($request, 'Create');
        if($request->file('categories_icon')){
            $icon = $this->rules->__upload($request->file('categories_icon'));
        } else {
            return back()->with('error', 'Icon tidak ditemukan');
        }
        $data = [
            'categories_name' => $request->categories_name,
            'categories_icon' => $icon
        ];
        $create = $this->repository->createData($data);
		if($create){
    		return redirect()->to(route('admin.category'))->with('success', 'Data kategori webinar berhasil ditambahkan');
    	}
    }
    public function edit(Request $request)
    {
        $data = $this->repository->getById($request->id);
        return view('pages.admin.webinar.category.edit', compact(['data']));
    }
    public function update(Request $request)
    {
        $this->rules->__rules($request, 'Update');
        if($request->file('categories_icon')){
            $icon = $this->rules->__upload($request->file('categories_icon'));
            $data = [
                'categories_name' => $request->categories_name,
                'categories_icon' => $icon
            ];
        } else {
            $data = [
                'categories_name' => $request->categories_name
            ];
        }
        $update = $this->repository->updateData($data, $request->id);
		if($update){
    		return redirect()->to(route('admin.category'))->with('success', 'Data kategori webinar berhasil diupdate');
    	}
    }
    public function destroy(Request $request)
    {
    	$delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
