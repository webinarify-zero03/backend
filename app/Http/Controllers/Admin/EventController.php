<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EventRepository;
use App\Repositories\CategoryRepository;
use App\Resources\Rules\EventRules;
use App\Resources\Responses\ApiResponse;
use DataTables;

class EventController extends Controller
{
    private $repoCategory, $repository, $rules, $response;
	public function __construct(EventRepository $repository, EventRules $rules, ApiResponse $response, CategoryRepository $repoCategory)
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
        $this->repoCategory = $repoCategory;
	}
    public function index()
    {
        return view('pages.admin.webinar.event.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getAll();
	        return Datatables::of($data)
	            ->addIndexColumn()
                ->addColumn('banner', function($row){
                    return '
                        <img src="'.$row['event_banner'].'" width="75" />
                    ';
                })
                ->addColumn('price_total', function($row){
                    return 'Rp '.number_format($row['price'], 2, ',','.');
                })
                ->addColumn('event_datetime', function($row){
                    return date('d-m-Y', strtotime($row['event_date'])).' '.date('H:i', strtotime($row['event_time']));
                })
	            ->addColumn('action', function($row){
	                return '
                        <a href="'.route('admin.event').'/view/'.$row['id'].'" class="btn btn-primary text-white"><i class="fas fa-eye"></i></a>
	                	<a href="'.route('admin.event').'/edit/'.$row['id'].'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['banner', 'price_total', 'event_datetime', 'action'])
	            ->make(true);
	    }
    }
    public function add()
    {
        $category = $this->repoCategory->getAll();
        if($category->count() > 0 ){
            return view('pages.admin.webinar.event.add', compact(['category']));
        } else {
            return redirect()->to(route('admin.category'))->with('error', 'Kategori masih kosong');
        }
    }
    public function store(Request $request)
    {
        $this->rules->__storeRules($request);
        $banner = $this->rules->__uploadPhoto($request->file('event_banner'));
        $sertifikat = $this->rules->__uploadPhoto($request->file('sertificate'));
        $data = [
            'user_id' => auth()->user()->id,
            'operator' => $request->operator,
            'category' => $request->category,
            'event_banner' => $banner,
            'sertificate' => $sertifikat,
            'title' => $request->title,
            'description' => $request->description,
            'benefits' => $request->benefits,
            'precondition' => $request->precondition,
            'event_date' => $request->event_date,
            'event_time' => $request->event_time,
            'execution' => $request->execution,
            'price' => $request->price,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'whatsapp_number' => $request->whatsapp_number,
            'event_status' => 'Registered',
        ];
        $create = $this->repository->createData($data);
        if($create) {
            return redirect()->to(route('admin.event'))->with('success', 'Data event webinar berhasil ditambahkan');
        }
    }
    public function view(Request $request)
    {
        $event = $this->repository->getByid($request->id);
        return view('pages.admin.webinar.event.view', compact(['event']));
    }
    public function edit(Request $request)
    {
        $category = $this->repoCategory->getAll();
        $event = $this->repository->getByid($request->id);
        if($category->count() > 0 ){
            return view('pages.admin.webinar.event.edit', compact(['event', 'category']));
        } else {
            return redirect()->to(route('admin.category'))->with('error', 'Kategori masih kosong');
        }
    }
    public function update(Request $request)
    {
        $this->rules->__updateRules($request);
        $event = $this->repository->getByid($request->id);
        if($request->file('event_banner')){
            $banner = $this->rules->__uploadPhoto($request->file('event_banner'));
        } else {
            $banner = $event->event_banner;
        }
        if($request->file('sertificate')){
            $sertifikat = $this->rules->__uploadPhoto($request->file('sertificate'));
        } else {
            $sertifikat = $event->sertificate;
        }
        $data = [
            'operator' => $request->operator,
            'event_banner' => $banner,
            'sertificate' => $sertifikat,
            'title' => $request->title,
            'description' => $request->description,
            'benefits' => $request->benefits,
            'precondition' => $request->precondition,
            'event_date' => $request->event_date,
            'event_time' => $request->event_time,
            'execution' => $request->execution,
            'price' => $request->price,
            'category' => $request->category,
            'email' => $request->email,
            'instagram' => $request->instagram,
            'whatsapp_number' => $request->whatsapp_number,
        ];
        $update = $this->repository->updateData($data, $request->id);
        if($update) {
            return redirect()->to(route('admin.event'))->with('success', 'Data event webinar berhasil diupdate');
        }
    }
    public function approve(Request $request)
    {
        if($request->approval == 'true'){
            $status = 'Registered';
        } else {
            $status = 'Rejected';
        }
        $data = [
            'event_status' => $status,
            'reason_reject' => $request->reason,
        ];
        $update = $this->repository->updateData($data, $request->id);
        if($update) {
            return redirect()->to(route('admin.event'))->with('success', 'Data event webinar berhasil diupdate');
        }
    }
    public function destroy(Request $request)
    {
        $delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
