<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

use App\Repositories\UserRepository;
use App\Resources\Rules\UserRules;
use App\Resources\Responses\ApiResponse;

use DataTables;

class UserController extends Controller
{
	private $repository, $rules, $response;
	public function __construct(UserRepository $repository, UserRules $rules, ApiResponse $response, )
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
	}
    public function index()
    {
    	return view('pages.admin.pengguna.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getByRole(2);
	        return Datatables::of($data)
	            ->addIndexColumn()
	            ->addColumn('photo', function($row){
	            	if($row['photo_profile']){
	            		return '<img src="'.$row['photo_profile'].'" width="50" height="50" class="rounded" />';
	            	} else {
	            		return '<img src="'.asset('').'assets/img/team-2.jpg" width="50" height="50" class="rounded" />';
	            	}
	            })
	            ->addColumn('status', function($row){
	            	if($row['email_verified_at']){
	            		return '<span class="text-success">Active</span>';
	            	} else {
	            		return '<span class="text-danger">Non-Active</span>';
	            	}
	            })
	            ->addColumn('action', function($row){
	                return '
	                	<a href="'.route('admin.users').'/show/'.$row['id'].'" class="btn btn-primary text-white"><i class="fas fa-eye"></i></a>
	                	<a href="'.route('admin.users').'/edit/'.$row['id'].'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['photo', 'status', 'action'])
	            ->make(true);
	    }
    }
    public function show(Request $request)
    {
    	$data = $this->repository->getById($request->id);
    	return view('pages.admin.pengguna.show', compact(['data']));
    }
    public function edit(Request $request)
    {
    	$data = $this->repository->getById($request->id);
    	return view('pages.admin.pengguna.edit', compact(['data']));
    }
    public function update(Request $request)
    {
    	$user = $this->repository->getById($request->id);
    	$this->rules->__rulesUpdate($request);
    	if($request->file('photo_profile')){
    		$photo = $this->rules->__uploadPhoto($request->file('photo_profile'));
    	} else {
    		$photo = $user->photo_profile;
    	}

    	$data = [
    		'photo_profile' => $photo,
	        'name' => $request->name,
	        'email' => $request->email,
	        'password' => $request->password,
	        'phone_number' => $request->phone_number,
	        'job' => $request->job,
	        'gender' => $request->gender,
    	];
    	$update = $this->repository->updateData($data, $request->id);
    	if($update){
    		return redirect()->to(route('admin.users'))->with('success', 'Data berhasil diupdate');
    	}
    }
    public function activated(Request $request)
    {
    	$user = $this->repository->getById($request->id);
    	if($user->markEmailAsVerified()){
            event(new Verified($user));
            return back()->with('success', 'Akun berhasil diaktifkan');
        }
    }
    public function destroy(Request $request)
    {
    	$delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
