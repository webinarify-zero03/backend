<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\TestimonyRepository;
use App\Resources\Rules\TestimonyRules;
use App\Resources\Responses\ApiResponse;

use DataTables;

class TestimonyController extends Controller
{
    private $repository, $rules, $response;
	public function __construct(TestimonyRepository $repository, TestimonyRules $rules, ApiResponse $response, )
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
	}
    public function index()
    {
    	return view('pages.admin.testimoni.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getAll();
	        return Datatables::of($data)
	            ->addIndexColumn()
                ->addColumn('text', function($row){
                    return substr($row['testimonies_text'], 0, 50).'...';
                })
	            ->addColumn('action', function($row){
	                return '
	                	<a href="'.route('admin.testimoni').'/edit/'.$row['id'].'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['text', 'action'])
	            ->make(true);
	    }
    }
    public function add()
    {
    	return view('pages.admin.testimoni.add');
    }
    public function store(Request $request)
    {
        $this->rules->__rules($request);
        $data = [
            'testimonies_name' => $request->testimonies_name,
            'testimonies_text' => $request->testimonies_text,
        ];
        $create = $this->repository->createData($data);
        if($create){
    		return redirect()->to(route('admin.testimoni'))->with('success', 'Data testimoni berhasil ditambahkan');
    	}
    }
    public function edit(Request $request)
    {
        $data = $this->repository->getById($request->id);
    	return view('pages.admin.testimoni.add', compact(['data']));
    }
    public function update(Request $request)
    {
        $this->rules->__rules($request);
        $data = [
            'testimonies_name' => $request->testimonies_name,
            'testimonies_text' => $request->testimonies_text,
        ];
        $update = $this->repository->updateData($data, $request->id);
        if($update){
    		return redirect()->to(route('admin.testimoni'))->with('success', 'Data testimoni berhasil diupdate');
    	}
    }
    public function destroy(Request $request)
    {
    	$delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
