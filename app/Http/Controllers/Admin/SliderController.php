<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\SliderEventRepository;
use App\Resources\Rules\SliderRules;
use App\Resources\Responses\ApiResponse;

use DataTables;

class SliderController extends Controller
{
	private $repository, $rules, $response;
	public function __construct(SliderEventRepository $repository, SliderRules $rules, ApiResponse $response, )
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
	}
    public function index()
    {
    	return view('pages.admin.slider.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getAll();
	        return Datatables::of($data)
	            ->addIndexColumn()
	            ->addColumn('photo', function($row){
	            	return '<img src="'.$row['sliders_picture'].'" class="rounded w-100" />';
	            })
	            ->addColumn('action', function($row){
	                return '
	                	<a href="'.route('admin.slider').'/edit/'.$row['id'].'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['photo', 'action'])
	            ->make(true);
	    }
    }
	public function add (Request $request)
	{
		return view('pages.admin.slider.add');
	}
	public function store (Request $request)
	{
		$this->rules->__storeRules($request);
		$photo = $this->rules->__uploadPhoto($request->file('sliders_picture'));
		$data = [
			'sliders_name' => $request->sliders_name,
			'sliders_picture' => $photo
		];
		$create = $this->repository->createData($data);
		if($create){
    		return redirect()->to(route('admin.slider'))->with('success', 'Data slider berhasil ditambahkan');
    	}
	}
	public function edit (Request $request)
	{
		$data = $this->repository->getById($request->id);
		return view('pages.admin.slider.edit', compact(['data']));
	}
	public function update (Request $request)
	{
		$this->rules->__updateRules($request);
		if($request->file('sliders_picture')){
			$photo = $this->rules->__uploadPhoto($request->file('sliders_picture'));
			$data = [
				'sliders_name' => $request->sliders_name,
				'sliders_picture' => $photo
			];
		} else {
			$data = [
				'sliders_name' => $request->sliders_name
			];
		}
		$update = $this->repository->updateData($data, $request->id);
		if($update){
    		return redirect()->to(route('admin.slider'))->with('success', 'Data slider berhasil diupdate');
    	}
	}
	public function destroy(Request $request)
    {
    	$delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
