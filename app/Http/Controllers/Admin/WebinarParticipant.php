<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ParticipantRepository;
use App\Repositories\EventRepository;
use App\Resources\Rules\EventRules;
use App\Resources\Responses\ApiResponse;

use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationPaymentMail;

use DataTables, PDF;

class WebinarParticipant extends Controller
{
    private $repoEvent, $repository, $rules, $response;
	public function __construct(ParticipantRepository $repository, EventRules $rules, ApiResponse $response, EventRepository $repoEvent)
	{
		$this->repository = $repository;
		$this->rules = $rules;
		$this->response = $response;
        $this->repoEvent = $repoEvent;
	}
    public function index(Request $request)
    {
        $event = $request->event_id;
        return view('pages.admin.webinar.event.participant.index', compact(['event']));
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getByEventId($request->event_id);
	        return Datatables::of($data)
	            ->addIndexColumn()
                ->addColumn('payment_image', function($row){
                    if(!$row['payment']){
                        return '
                            <em class="text-danger">Belum melakukan pembayaran</em>
                        ';
                    } else {
                        return '
                            <img src="'.$row['payment'].'" width="75" />
                        ';   
                    }
                })
                ->addColumn('price', function($row){
                    return 'Rp '.number_format($row['price_total'], 2, ',','.');
                })
	            ->addColumn('action', function($row){
                    if( $row['status'] == 'Waiting Approval' ){
                        return '
                            <a href="javascript:approveData('.$row['id'].', 1)" class="btn btn-primary text-white">Setujui Pembayaran</a>
                            <a href="javascript:approveData('.$row['id'].', 0)" class="btn btn-danger text-white">Tolak Pembayaran</a>
                        ';
                    } else if ( $row['status'] == 'Rejected' ){
                        return '
                            <em class="text-danger">Data ditolak</em><br />
                            <a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white">Hapus Data</a>
                        ';
                    } else if ( $row['status'] == 'Approve' ){
                        return '
                            <em class="text-success">Data Disetujui</em><br />
                            <a href="'.route('event.participant').'/'.$row['event_id'].'/sertificate/'.$row['id'].'" target="__blank" class="btn btn-primary text-white">Lihat Sertifikat</a>
                        ';
                    } else {
                        return '<em>'.$row['status'].'</em>';
                    }
	            })
	            ->rawColumns(['price', 'payment_image', 'action'])
	            ->make(true);
	    }
    }
    public function confirmation(Request $request)
    {
        $participant = $this->repository->getById($request->id);
        $event = $this->repoEvent->getById($request->event_id);
        if( $request->status == 0 ){
            $message = 'Pembayaran anda terhadap webinar dengan judul " '.$event->title.' " ditolak';
            $update = $this->repository->updateData(['status' => 'Rejected'], $request->id);
        } else {
            $message = 'Pembayaran anda terhadap webinar dengan judul " '.$event->title.' " disetujui, silahkan hubungi admin untuk melakukan konfirmasi ulang';
            $update = $this->repository->updateData(['status' => 'Approve'], $request->id);
        }
        Mail::to($participant->email_participant)->send(new ConfirmationPaymentMail($message));

        if($update){
    		return $this->response->successDelete('Berhasil mengupdate data', 200);
    	}
    }
    public function sertificate(Request $request)
    {
        $event = $this->repoEvent->getById($request->event_id);
        $participant = $this->repository->getById($request->id);
    
        $data = [
            'event' => $event,
            'participant' => $participant
        ];
    
        $pdf = PDF::loadView('pages.admin.webinar.event.participant.sertificate', $data);
    
        $pdf->setPaper('A4', 'Landscape');
        $pdf->render();
        return $pdf->stream('sertificate.pdf');
    }
    public function destroy(Request $request)
    {
        $delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
