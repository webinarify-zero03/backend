<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\RekeningRepository;
use App\Resources\Responses\ApiResponse;
use DataTables;


class RekeningController extends Controller
{
    private $repository, $rules, $response;
	public function __construct(RekeningRepository $repository, ApiResponse $response)
	{
		$this->repository = $repository;
		$this->response = $response;
	}
    public function index()
    {
        return view('pages.admin.rekening.index');
    }
    public function json(Request $request)
    {
    	if($request->ajax()){
	    	$data = $this->repository->getAll();
	        return Datatables::of($data)
	            ->addIndexColumn()
	            ->addColumn('action', function($row){
	                return ' 
	                	<a href="javascript:updateData('.$row['id'].')" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
	                	<a href="javascript:deleteData('.$row['id'].')" class="btn btn-danger text-white"><i class="fas fa-trash"></i></a>
	                ';
	            })
	            ->rawColumns(['action'])
	            ->make(true);
	    }
    }
    public function view(Request $request)
    {
        $data = $this->repository->getById($request->id);
        return response()->json(['data' => $data]);
    }
    public function update(Request $request)
    {
        if($request->type == 'store'){
            $data = [
                'rekening_number' => $request->rekening_number,
                'rekening_type' => $request->rekening_type,
                'rekening_name' => $request->rekening_name,
            ];
            $create = $this->repository->createData($data);
            if($create){
                return back()->with('success', 'Data berhasil ditambahkan');
            }
        } else {
            $data = [
                'rekening_number' => $request->rekening_number,
                'rekening_type' => $request->rekening_type,
                'rekening_name' => $request->rekening_name,
            ];
            $update = $this->repository->updateData($data, $request->id);
            if($update){
                return back()->with('success', 'Data berhasil diupdate');
            }
        }
    }
    public function destroy(Request $request)
    {
        $delete = $this->repository->deleteData($request->id);
    	if($delete){
    		return $this->response->successDelete('Berhasil menghapus data', 200);
    	}
    }
}
