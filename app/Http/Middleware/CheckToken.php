<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Resources\Responses\ApiResponse;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */

    private $rules;
    public function __construct(ApiResponse $response)
    {
        $this->response = $response;
    }

    public function handle(Request $request, Closure $next): Response
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            return $next($request);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $this->response->errorResponse('Token telah kadaluarsa', 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->response->errorResponse('Token salah', 401);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $this->response->errorResponse('Token tidak ditemukan', 401);
        }
    }
}
