<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('operator');
            $table->string('event_banner');
            $table->string('title');
            $table->text('description');
            $table->text('benefits');
            $table->text('precondition');
            $table->date('event_date');
            $table->time('event_time');
            $table->string('execution');
            $table->string('price');
            $table->string('email');
            $table->string('instagram')->nullable();
            $table->string('whatsapp_number');
            $table->BigInteger('category');
            $table->string('event_status');
            $table->string('reason_reject')->nullable();
            $table->string('sertificate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
