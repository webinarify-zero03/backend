<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\About::create([
            'logo' => 'http://localhost:8000/assets/images/logo.png',
            'instagram' => 'webinarify.id',
            'whatsapp' => '08155146001',
            'email' => 'webinarify@gmail.com',
            'short_description' => 'Layanan penyedia kegiatan webinar yang mudah dan luas yang ada di nusantara',
            'long_description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Incidunt assumenda sequi aperiam impedit odit quaerat doloremque nobis, ab labore iste nihil eaque, a optio corrupti cupiditate est natus obcaecati quas?',
        ]);
    }
}
